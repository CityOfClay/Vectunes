#ifndef AudProject_hpp
#define AudProject_hpp

#include "LoopSong.hpp"
#include "FileMapper.hpp"

/**
 *
 * @author MultiTool
*/
class AudProject: public IDeletable {
public:
  OffsetBoxBase* AudioRoot;
  GraphicBox::Graphic_OffsetBox* GraphicRoot = nullptr;
  GraphicBox* GBox;
  int SampleRate = Globals::SampleRate;
  int UpdateCounter = 1;
  StringConst TreePhraseName = "Tree", LibraryPhraseName = "Library";// for serialization
  double VersionNum = 0.1;// song file version
  StringConst VersionNumName = "Version";// for serialization
  Config conf;
  /* ********************************************************************************* */
  AudProject() {}
  /* ********************************************************************************* */
  void Wrap_For_Graphics(OffsetBoxBase& obox) {}
  /* ********************************************************************************* */
  void Update_Guts() {
    MetricsPacket metrics;
    metrics.MaxDuration = 0.0;
    metrics.MyProject = &(this->conf);
    metrics.FreshnessTimeStamp = UpdateCounter++;// to do: increment this every time it is called.
    this->GBox->Update_Guts(metrics);
  }
  /* ********************************************************************************* */
  void Create_For_Graphics() {}
  /* ********************************************************************************* */
  void Compose_Test() {}
  /* ********************************************************************************* */
  void Audio_Test() {}
  /* ********************************************************************************* */
  void Render_Test(const String &FileName) {
    ISonglet *songlet = this->AudioRoot->GetContent();
    double Duration = songlet->Get_Duration();
    SingerBase *singer = this->AudioRoot->Spawn_Singer();
    Wave wav;
    wav.Assign_SampleRate(Globals::SampleRate);
    singer->Render_To(Duration, wav);
    wav.SaveToWav(FileName);
  }
  /* ********************************************************************************* */
  String Textify() {// serialize  NOT FUNCTIONAL YET
    JsonParse::HashNode *MainPhrase = new JsonParse::HashNode();
    CollisionLibrary HitTable;// = new ITextable::CollisionLibrary();
    //JsonParse::HashNode *Tree = this->GraphicRoot.Export(HitTable);// to do: enable all this Tree stuff
    JsonParse::HashNode *Library = HitTable.ExportJson();
    MainPhrase->AddSubPhrase(VersionNumName, ITextable::PackNumberField(this->VersionNum));
    //MainPhrase->AddSubPhrase(TreePhraseName, Tree);
    MainPhrase->AddSubPhrase(LibraryPhraseName, Library);
    String Json = MainPhrase->ToJson();
    delete MainPhrase;
    return Json;
  }
  /* ********************************************************************************* */
  void UnTextify(String& JsonTxt) {// deserialize  NOT FUNCTIONAL YET
    JsonParse jparse;

    // RootJNode will be deleted by jparse's destructor. So use it only in this scope!
    JsonParse::HashNode *RootJNode = jparse.Parse(JsonTxt);

    this->VersionNum = ITextable::GetNumberField(*RootJNode, VersionNumName, -1.0);// -1 default means error

    GraphicBox::Graphic_OffsetBox* GraphicRootTemp;
    FileMapper fmap;
    GraphicRootTemp = fmap.Create(RootJNode);
    if (GraphicRootTemp!=nullptr){
      delete this->GraphicRoot;
      this->GraphicRoot = GraphicRootTemp;

      this->GBox = this->GraphicRoot->Content;

      this->AudioRoot = this->GBox->ContentOBox;
      this->Update_Guts();
      this->GraphicRoot->UpdateBoundingBox();// snox shouldn't this be inside Update_Guts?
    }else{
      std::cout << "Error parsing file.\n";
    }
  }
  /* ********************************************************************************* */
  bool ReadFile(const String &FilePath) {
    using namespace std;
    String JsonTxt;
    string line;
    ifstream myfile(FilePath);
    if (myfile.is_open()){
      while (getline(myfile,line)) {
        cout << line << '\n';
        JsonTxt += line;
      }
      myfile.close();
      this->UnTextify(JsonTxt);
      return true;
    } else {
      cout << "Jsong file is not open!\n";
      return false;
    }
  }
  /* ********************************************************************************* */
  boolean Create_Me() { return 0; }
  void Delete_Me() {}
};


#endif
