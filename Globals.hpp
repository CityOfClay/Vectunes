#ifndef Globals_hpp
#define Globals_hpp

// C++ stuff
#define _USE_MATH_DEFINES
#include <cmath>
#include <climits>
#include <limits>       // std::numeric_limits
#include <algorithm>    // std::min
#include <stdlib.h>     // srand, rand
#include <cstdlib> // RAND_MAX
#include <string>
#include <vector>
#include <map>
#include <string_view>

#if true
 #define ldouble long double
#else
 #define ldouble double
#endif // false

#if false
 #define SoundFloat long double
#else
 #define SoundFloat double
#endif // false

#define boolean bool
#define jpublic
#define jprivate
#define implements :
#define interface class
#define extends :
#define Object void*
#define null nullptr
//#define ArrayList std::vector
//#define HashMap std::unordered_map
//#define HashMap std::map

#if true // string_view is the right way, but it messes up debugging for some reason
  #define StringConst static inline const std::string
#else
  #define StringConst static constexpr std::string_view
#endif

#define Math_min(a, b) (std::min(a, b))
#define Math_max(a, b) (std::max(a, b))
#define Math_ceil(a) (std::ceil(a))
#define Math_abs(a) (std::abs(a))
#define Math_hypot(a, b) (std::hypot(a, b))

#define Double_POSITIVE_INFINITY std::numeric_limits<SoundFloat>::max()
#define Double_NEGATIVE_INFINITY std::numeric_limits<SoundFloat>::min()
#define Integer_MIN_VALUE INT_MIN
#define Integer_MAX_VALUE INT_MAX

#define PIdef 3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798214808651328230664709384460955058223172535940812848111745028410270L
#define PI2def (PIdef*2.0) //M_PI_2;

/**
 *
 * @author MultiTool
*/

class Globals {
public:
  const static int SampleRate = 44100;
  //const int SampleRate = 44100;
  const static int SampleRateTest = 100;
  static constexpr SoundFloat BaseFreqC0 = 16.3516;// hz
  static constexpr SoundFloat BaseFreqA0 = 27.5000;// hz
  static constexpr SoundFloat MiddleC4Freq = 261.626;// hz
  //static constexpr SoundFloat BaseFreqC0;// hz
  //const constexpr BaseFreqC0 = 16.3516;// hz
  //static SoundFloat BaseFreqA0;// = 27.5000;// hz
  //constexpr static SoundFloat usPerSec = 1000000.0;

  static constexpr auto ObjectTypeName = "ObjectTypeName";// for serialization

  // http://stackoverflow.com/questions/2777541/static-const-SoundFloat-in-c
  static SoundFloat BaseFreqA0_G() {
    return 27.5000;  // hz
  }
  static SoundFloat BaseFreqC0_G() {
    return 16.3516;  // hz
  }

  static constexpr long double Math_PI = PIdef;
  static constexpr long double TwoPi = PI2def; //M_PI_2; // hz
  static constexpr SoundFloat Fudge = 0.00000000001;

  class Random {
  public:
    SoundFloat NextDouble() {
      return ((SoundFloat)rand()) / (SoundFloat)RAND_MAX;
    }
  };
  const static Random RandomGenerator();
};

struct Example {
  static SoundFloat usPerSec() {
    return 1000000.0;
  }
};

#endif
