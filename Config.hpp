#ifndef Config_hpp
#define Config_hpp

#include "Globals.hpp"

/**
 *
 * @author MultiTool
 *
 */
class Config {// general config settings used by project instance
public:
  int SampleRate = Globals::SampleRate;
  ~Config(){ this->SampleRate=Integer::MIN_VALUE; }// wreck on delete
};

#endif // Config_hpp

