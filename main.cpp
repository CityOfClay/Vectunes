#include <iostream>
#include <time.h>
#include <chrono>
#include <ctime>
#include <fstream>
#include <experimental/filesystem>
//#include <filesystem>
#include <sys/types.h>
#include <dirent.h>

#include "Vectunes.hpp"

using namespace std;

/*
quehacers:
get rid of Project/Config/SampleRate reference in all singers. pass SampleRate as a render-time parameter in wave or Start().
refine SampleVoice
import PluckVoice
import GraphicBox?
create LoopSong factory and exporter
finish text/untext tree/graphicbox of audproject

*/

/* ********************************************************************************* */
void Convert(const String &FName) {
  AudProject aup;
  if (aup.ReadFile(FName)) {
    aup.Render_Test(FName + ".wav");
  }
}
/* ********************************************************************************* */
void Scan(const std::string &path) {// https://stackoverflow.com/questions/612097/how-can-i-get-the-list-of-files-in-a-directory-using-c-or-c
  namespace fs = std::experimental::filesystem;
//  for (const auto &entry : fs::directory_iterator(path)){
//    std::cout << entry.path() << std::endl;
//  }
}

bool HasEnding(std::string const &FullString, std::string const &ending) {
  if (FullString.length() >= ending.length()) { // https://stackoverflow.com/questions/874134/find-if-string-ends-with-another-string-in-c
    return (0 == FullString.compare(FullString.length() - ending.length(), ending.length(), ending));
  } else {
    return false;
  }
}

void Scan2(const char *name) {
  int len = strlen(name);
  DIR *dirp = opendir("./");
  dirent *dp;
  while ((dp = readdir(dirp)) != NULL) {
    cout << dp->d_name << "\n";
//    if (dp->d_namlen == len && !strcmp(dp->d_name, name)) {
//      cout << "Found:" << dp->d_name << "\n";
//      //(void)closedir(dirp);
//      //return FOUND;
//    }
  }
  (void)closedir(dirp);
  //return NOT_FOUND;
}
void read_directory(const std::string &name, std::vector<String> &txtvec) { // http://www.martinbroadhurst.com/list-the-files-in-a-directory-in-c.html
  DIR *dirp = opendir(name.c_str());
  struct dirent *dp;
  while ((dp = readdir(dirp)) != NULL) {
//    std::string *str = new std::string(dp->d_name);
//    txtvec.push_back(*str);
    txtvec.push_back(dp->d_name);
    if  (HasEnding(dp->d_name, ".jsong")){
      Convert(std::string(dp->d_name));
    }
  }
  closedir(dirp);
}

/* ********************************************************************************* */
int main() {
  std::srand(std::time(nullptr)); // use current time as seed for random generator
  Voice_Iterative = false;// temporary for testing

  if (true) {
    // Scan("./"); Scan2("./");
    std::vector<String> txtvec;
    read_directory("./", txtvec);
    //return 0;
//    Convert("./AnyTest.jsong");
//    Convert("./simple3.jsong");
//    Convert("./slormbeat03.jsong");
//    Convert("./Depeche03.jsong");
//    Convert("./groans04.jsong");
//    Convert("./horn_chords_long01.jsong");
    //return 0;
  }

  if (false) {
    cout << MonkeyBox::TimeXName << "\n";
    cout << MonkeyBox::OctaveYName << "\n";
    cout << MonkeyBox::ScaleXName << "\n";
    double test = 10;
    test = Double::ParseDouble("0.1", 11);
    printf("test:%f\n", test);
    test = Double::ParseDouble("abc", 12);
    printf("test:%f\n", test);
    // return 0;
  }

  if (false) {
    Tokenizer tkzer;
    String JsonTxt = "{ \"Hello\" : \"World\",  \"AlohaMars\" : { \"Bienvenidos\" : \"Phobos\" },  \"Howdy\" : \"Pluto\" }";
    ArrayList<Token*>* Tree = nullptr;
    printf("Tokenizer.Tokenize\n");
    Tree = tkzer.Tokenize(0, JsonTxt);
    printf("Tokenizer.Tokenize done\n");
    tkzer.Print_Tokens();

    JsonParse jp;
    jp.Parse(JsonTxt);
    //jp.Fold(*Tree);
    //return 0;
  }

  if (false) {
    Functor f;
    int i = f(3.14);
    return 0;
  }
  if (false) {
    ComparePowers();
    return 0;
  }
  Config conf;
  MetricsPacket metrics;
  metrics.MaxDuration = 0.0;
  metrics.MyProject = &conf;
  metrics.FreshnessTimeStamp = 2;

  if (false) {
    Wave wav;
    wav.Assign_SampleRate(Globals::SampleRate);
    SampleVoice svoice;
    printf("SampleVoice.Preset_Horn();\n");
    svoice.Preset_Horn();
    FillVoice(&svoice);
    svoice.Update_Guts(metrics);
    svoice.Set_Project(&conf);
    printf("SampleVoice.Spawn_OffsetBox();\n");
    SampleVoice::SampleVoice_OffsetBox *svobox = svoice.Spawn_OffsetBox();
    SampleVoice::SampleVoice_Singer *svsinger;
    printf("SampleVoice_Singer.Spawn_Singer();\n");
    svsinger = svobox->Spawn_Singer();
    svsinger->Start();
    printf("SampleVoice_Singer.Render_To();\n");
    svsinger->Render_To(1.0, wav);
    delete svsinger;
    printf("wav.SaveToWav\n");
    wav.SaveToWav("Horn.wav");
    //delete svobox;
    //return 0;
  }

  if (false) {
    LoopSong* tree = PatternMaker::MakeRandom();
    GroupSong *grsong = new GroupSong();
    GroupSong::Group_OffsetBox *treebox = tree->Spawn_OffsetBox();
    treebox->OctaveY = 3.0;
    grsong->Add_SubSong(treebox);

    grsong->Update_Guts(metrics);
    grsong->Set_Project(&conf);// finalize

    GroupSong::Group_OffsetBox *grobox = grsong->Spawn_OffsetBox();
    GroupSong::Group_Singer *gsing = grobox->Spawn_Singer();
    Wave wave;
    wave.Assign_SampleRate(Globals::SampleRate);
    gsing->Start();
    printf("Render_To:\n");
    gsing->Render_To(tree->Duration, wave);
    printf("Render_To done.\n");
    delete gsing;
    printf("SaveToWav:\n");
    wave.SaveToWav("RandomTree.wav");
    printf("SaveToWav done.\n");

    Voice_Iterative = true;// temporary for testing
    TestSpeed(*grsong, 3);
    Voice_Iterative = false;// temporary for testing
    TestSpeed(*grsong, 3);
    delete grobox;

    return 0;
  }

  if (true) {
    CreateBentTriad();
    //return 0;
  }

  if (false) { // create bent minor triad
    Voice *voz;
    if (true) {
      voz = PatternMaker::Create_Bent_Note(0.0, 1.0, 0.0, 1.0);
    } else {
      voz = new Voice();
      FillVoice(voz, 1.0);// add voice bend points
    }

    GroupSong *triad = PatternMaker::MakeMinor(*voz, 12 * 5);
    triad->Update_Guts(metrics);
    triad->Set_Project(&conf);

    Wave wave;
    wave.Assign_SampleRate(Globals::SampleRate);
    GroupSong::Group_OffsetBox *triadbox = triad->Spawn_OffsetBox();
    GroupSong::Group_Singer *gsing = triadbox->Spawn_Singer();
    gsing->Start();
    gsing->Render_To(triad->Duration, wave);
    delete gsing;

    Voice_Iterative = true;// temporary for testing
    TestSpeed(*triad, 3);
    Voice_Iterative = false;// temporary for testing
    TestSpeed(*triad, 3);

    wave.SaveToWav("Minor.wav");

    delete triadbox;
  }

  if (true) {// Voice and  Group
    Voice *voz;
    voz = new Voice();
    voz->Update_Guts(metrics);
    voz->Set_Project(&conf);

    //FillVoice(voz, 2000.0);// add voice bend points, 33 min 20 sec
    FillVoice(voz, 1.0);// add voice bend points, 1 min
    //FillVoice(voz, 0.2);// add voice bend points

    Voice::Voice_OffsetBox *vobox = voz->Spawn_OffsetBox();
    vobox->TimeX = 0.27;//0.11;// 0.0;
    //vobox->TimeX = 0.0;
    if (true) {
      Voice::Voice_Singer *vsing = vobox->Spawn_Singer();

      //Voice::Voice_Singer *vsing = voz->Spawn_Singer();
      cout << "Current_Frequency:" << vsing->Current_Frequency << endl;
      vsing->Start();
      Chop_Test(vsing, "Voice");
      //Skip_Test(vsing, "Voice");
      delete vsing;
    }
    vobox->OctaveY = 2.0;
    GroupSong *gsong = new GroupSong();
    gsong->Add_SubSong(vobox);

    metrics.Reset();
    gsong->Update_Guts(metrics);
    gsong->Set_Project(&conf);

    GroupSong::Group_OffsetBox *grobox = gsong->Spawn_OffsetBox();
    //grobox->Set_Project(&conf);
    //ISonglet::Unref(gb);
    //delete gb;// automatically deleted by grobox

    GroupSong::Group_Singer *gsing = grobox->Spawn_Singer();
    Chop_Test(gsing, "Group");
    //Skip_Test(gsing, "Group");
    delete gsing;
    delete grobox;
    //delete vobox;// the group already deleted everything.

    // delete voz;// voice is deleted automatically when we delete vobox
  }

  if (false) {// Span - simple group with one delayed voice
    Voice *voz0;
    voz0 = new Voice();
    FillVoice(voz0);

    Voice::Voice_OffsetBox *vobox0 = voz0->Spawn_OffsetBox();
    vobox0->TimeX = 0.27;//0.11;

    GroupSong *gsong = new GroupSong();
    gsong->Add_SubSong(vobox0);

    gsong->Set_Project(&conf);
    gsong->Update_Guts(metrics);

    GroupSong::Group_OffsetBox *gobox = gsong->Spawn_OffsetBox();

    SingerBase *singer = gobox->Spawn_Singer();
    Chop_Test(singer, "Span");

    delete gsong;
  }

  if (false) {// Loop
    Voice *voz0;
    voz0 = new Voice();
    FillVoice(voz0);
    Voice::Voice_OffsetBox *vobox0 = voz0->Spawn_OffsetBox();

    GroupSong *chord = MakeChord(voz0);
    GroupSong::Group_OffsetBox *ChordHandle = chord->Spawn_OffsetBox();

    LoopSong *lsong = new LoopSong();

    //lsong->Add_SubSong(vobox0);
    lsong->Add_SubSong(ChordHandle);

    lsong->Set_Beats(100);
    //lsong->Set_Interval(0.10);// exposes loop/group chopping bug
    lsong->Set_Interval(0.25);
    GroupSong::Group_OffsetBox *lobox = lsong->Spawn_OffsetBox();

    lsong->Set_Project(&conf);
    lsong->Update_Guts(metrics);

    SingerBase *singer = lobox->Spawn_Singer();

    Chop_Test(singer, "Loop");
    delete singer;

    TestSpeed(*lsong);
    //finished loop computation at Thu Nov 08 09:30:09 2018
    //elapsed time: 4.09459s

    delete lsong;
  }
  if (false) {
    Outer *ouch = new Outer();
    Outer::Inner *een = ouch->Spawn();
    cout << "een->NumberIn:" << een->NumberIn << endl;
    delete een;
    delete ouch;
  }

  if (false) {
    GroupSong *gb1 = new GroupSong();
    GroupSong::Group_OffsetBox *grobox1 = gb1->Spawn_OffsetBox();
    delete grobox1;
    //ISonglet::Unref(gb);
    //delete gb;// automatically deleted by grobox
  }

  OffsetBoxBase *obox = new OffsetBoxBase();

  OffsetBoxBase *oboxkid = obox->Clone_Me();

  if (false) {
    Wave *wav = new Wave();
    cout << "Hello world!" << endl;
    delete wav;
  }
  delete obox;
  delete oboxkid;

  return 0;
}
