#ifndef OffsetBoxBase_hpp
#define OffsetBoxBase_hpp

#include "Globals.hpp"
#include "MonkeyBox.hpp"
#include "ISonglet.hpp"

class CajaDelimitadora;// forward
class SingerBase;// forward

class Factory;// forward
class OffsetBoxBase: public MonkeyBox { // IMoveable, IDeletable {// location box to transpose in pitch, move in time, etc.
public:
  Factory* MyFactory = InitFactory();
  StringConst ContentName = "Content";
  ISonglet *MySonglet;
  /* ********************************************************************************* */
  OffsetBoxBase() {
    this->Create_Me();
    this->MyBounds.Reset();
  }
  virtual ~OffsetBoxBase(){this->Delete_Me();}
  /* ********************************************************************************* */
  OffsetBoxBase* Clone_Me() override {// ICloneable
    OffsetBoxBase *child = new OffsetBoxBase();// clone
    child->Copy_From(*this);
    return child;
  }
  /* ********************************************************************************* */
  OffsetBoxBase* Deep_Clone_Me(CollisionLibrary& HitTable) override {// ICloneable
    OffsetBoxBase *child = this->Clone_Me();
    return child;
  }
  /* ********************************************************************************* */
  virtual void BreakFromHerd(CollisionLibrary& HitTable) {}
  /* ********************************************************************************* */
  void Copy_From(const OffsetBoxBase& donor) {
    MonkeyBox::Copy_From(donor);
  }
  /* ********************************************************************************* */
//  SoundFloat Get_Max_Amplitude() override {
//    return 0;
//  }
  SoundFloat Get_Max_Amplitude() override {// always override this
    return this->LoudnessFactor;
  }
  /* ********************************************************************************* */
  virtual void Rebase_Time(SoundFloat Time) {}
  /* ********************************************************************************* */
  virtual SingerBase* Spawn_Singer() {// always always always override this
    throw std::runtime_error("Not supported yet.");
    return nullptr;
  }
  /* ********************************************************************************* */
  virtual void Rescale_TimeX(SoundFloat Factor) {
    this->ScaleX = Factor;
  }
  /* ********************************************************************************* */
  virtual ISonglet* GetContent() {// always always always override this
    return this->MySonglet;
    //throw std::runtime_error("Not supported yet.");
    //return nullptr;
  }
  // <editor-fold defaultstate="collapsed" desc="IDrawable and IMoveable">
  /* ********************************************************************************* */
  void Draw_Me(IDrawingContext& ParentDC) override {// IDrawable
  }
  /* ********************************************************************************* */
  virtual void Draw_Dot(IDrawingContext& DC, Color& col) {}
  /* ********************************************************************************* */
  virtual void Draw_My_Bounds(IDrawingContext& ParentDC) {}
  /* ********************************************************************************* */
//  virtual CajaDelimitadora* GetBoundingBox() {// IDrawable, not needed for monkeybox, fix this
//    return &(this->MyBounds);
//  }
  void UpdateBoundingBox() override {// IDrawable
//    ISonglet *Content = this->GetContent();
//    Content->UpdateBoundingBox();
//    this->UpdateBoundingBoxLocal();
  }
  void UpdateBoundingBoxLocal() override {// IDrawable
//    ISonglet *Content = this->GetContent();
//    Content->UpdateBoundingBoxLocal();// either this
//    this->UnMap(Content->GetBoundingBox(), MyBounds);// project child limits into parent (my) space
//    // include my bubble in bounds
//    this->MyBounds.IncludePoint(this->TimeX - OctavesPerRadius, this->OctaveY - OctavesPerRadius);
//    this->MyBounds.IncludePoint(this->TimeX + OctavesPerRadius, this->OctaveY + OctavesPerRadius);
  }
  void GoFishing(IGrabber& Scoop) override {}
  void MoveTo(SoundFloat XLoc, SoundFloat YLoc) override {}
  SoundFloat GetX() override {return this->TimeX;}
  SoundFloat GetY() override {return this->OctaveY;}
  boolean HitsMe(SoundFloat XLoc, SoundFloat YLoc) override {// IDrawable.IMoveable
    //System.out.print("HitsMe:");
    if (this->MyBounds.Contains(XLoc, YLoc)) {// redundant test
      SoundFloat dist = Math::hypot(XLoc - this->TimeX, YLoc - this->OctaveY);
      if (dist <= this->OctavesPerRadius) {
        //System.out.println("true");
        return true;
      }
    }
    //System.out.println("false");
    return false;
  }
  // </editor-fold>
  /* ********************************************************************************* */
  boolean Create_Me() override {// not needed probably
    return MonkeyBox::Create_Me();
  }
  void Delete_Me() override { MonkeyBox::Delete_Me(); }
  /* ********************************************************************************* */
  JsonParse::HashNode* Export(CollisionLibrary& HitTable) override {// ITextable
    JsonParse::HashNode *SelfPackage = MonkeyBox::Export(HitTable);// ready for test?
    JsonParse::Node *ChildPackage = nullptr;
    ISonglet *songlet = this->GetContent();
    if (songlet->GetRefCount() != 1) {// songlet exists in more than one place, use a pointer to library
      JsonParse::LiteralNode *litnode = new JsonParse::LiteralNode();// multiple references, use a pointer to library instead
      CollisionItem *colitem;// songlet is already in library, just create a child phrase and assign its textptr to that entry key
      if ((colitem = HitTable.GetItem(songlet)) == nullptr) {
        colitem = HitTable.InsertUniqueInstance(songlet);// songlet is NOT in library, serialize it and add to library
        colitem->JsonPhrase = songlet->Export(HitTable);
      }
      litnode->Literal = colitem->ItemTxtPtr;
      ChildPackage = litnode;// multiple references, use a pointer to library instead
    } else {// songlet only exists in one place, make it inline.
      ChildPackage = songlet->Export(HitTable);
    }
    SelfPackage->AddSubPhrase(OffsetBoxBase::ContentName, ChildPackage);
    return SelfPackage;
  }
  void ShallowLoad(JsonParse::HashNode& phrase) override {
    MonkeyBox::ShallowLoad(phrase);
  }
  void Consume(JsonParse::HashNode& phrase, CollisionLibrary& ExistingInstances) override {}
  virtual ISonglet* Spawn_And_Attach_Songlet() {// inverted birth
    return nullptr;
  }
  /* ********************************************************************************* */
  Factory* InitFactory() {// for serialization
    return nullptr;
  }
#if false
  /* ********************************************************************************* */
  class Factory: public IFactory {// for serialization
  public:
    OffsetBox* Create(JsonParse::Node& phrase, CollisionLibrary& ExistingInstances) {
      return nullptr;
    }
  };
#endif // false

};

#endif // OffsetBoxBase_hpp
