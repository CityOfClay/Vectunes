#ifndef GraphicBox_hpp
#define GraphicBox_hpp

#include "ICloneable.hpp"
#include "LoopSong.hpp"

/**
 *
 * @author MultiTool
*/
class AudProject;// forward

class GraphicBox: public ISonglet, public IDeletable {// , public IDrawable
public:
  OffsetBoxBase* ContentOBox = null;
  IMoveable* Floater = null;
  CajaDelimitadora MyBounds;
  int FreshnessTimeStamp;
  StringConst ContentOBoxName = "ContentOBox";
  /* ********************************************************************************* */
  class Artist {
  public:
    GraphicBox *MySonglet = nullptr;
    Artist(){ }
    Artist(GraphicBox *MySonglet) {
      this->MySonglet = MySonglet;
    }
    virtual void Draw_Me(IDrawingContext& ParentDC) {
    }
    virtual bool HitsMe(Grabber &grabber) { return false; }
    virtual void GoFishing(Grabber& Scoop) {}
  };
  /* ********************************************************************************* */
  class ArtistFactory { // single-instance for all of this type of songlet
  public:
    virtual Artist* SpawnArtist() const { return new Artist(); }
  };
  static ArtistFactory *ArtistMaker;// = nullptr;
  Artist *MyArtist = nullptr;
  /* ********************************************************************************* */
  class Graphic_OffsetBox: public OffsetBoxBase {
  public:
    GraphicBox *Content;
    StringConst ObjectTypeName = "Graphic_OffsetBox";
    /* ********************************************************************************* */
    Graphic_OffsetBox() {}
    /* ********************************************************************************* */
    void Attach_Content(OffsetBoxBase *content) {
      this->Content->Attach_Content(content);
    }
    /* ********************************************************************************* */
    GraphicBox* GetContent() { return nullptr; }
    /* ********************************************************************************* */
    void Attach_Songlet(GraphicBox *songlet) {// for serialization
      this->MySonglet = this->Content = songlet;
      songlet->Ref_Songlet();
    }
    /* ********************************************************************************* */
    void Draw_Me(IDrawingContext& ParentDC) override {}
    void UpdateBoundingBox() override {}
    void UpdateBoundingBoxLocal() override {}
    void GoFishing(Grabber& Scoop) {}
    /* ********************************************************************************* */
    void MoveTo(double XLoc, double YLoc) override {}
    /* ********************************************************************************* */
    Graphic_OffsetBox* Clone_Me() override { return nullptr; }
    /* ********************************************************************************* */
    Graphic_OffsetBox* Deep_Clone_Me(CollisionLibrary& HitTable) override { return nullptr; }
    /* ********************************************************************************* */
    void BreakFromHerd(CollisionLibrary& HitTable) override {}
    /* ********************************************************************************* */
    boolean Create_Me() override { return 0; }
    void Delete_Me() override {}
    /* ********************************************************************************* */
    JsonParse::HashNode* Export(CollisionLibrary& HitTable) { return nullptr; }
    void ShallowLoad(JsonParse::HashNode& phrase) override {
      OffsetBoxBase::ShallowLoad(phrase);
    }
    void Consume(JsonParse::HashNode& phrase, CollisionLibrary& ExistingInstances) {}
    ISonglet* Spawn_And_Attach_Songlet() { return nullptr; }
    /* ********************************************************************************* */
    void Zoom(double XCtr, double YCtr, double Scale) {}
    /* ********************************************************************************* */
//    class Factory: public IFactory {// for serialization
//    public:
//      Graphic_OffsetBox* Create(JsonParse::Node& phrase, CollisionLibrary& ExistingInstances) { return nullptr; }
//    };
  };// Graphic_OffsetBox
  /* ********************************************************************************* */
  GraphicBox(){
    if (GraphicBox::ArtistMaker!=nullptr){
      this->MyArtist = GraphicBox::ArtistMaker->SpawnArtist();
      this->MyArtist->MySonglet = this;
     //GraphicBox.hpp|94|error: passing 'const GraphicBox::ArtistFactory' as 'this' argument discards qualifiers [-fpermissive]|
    }
  }
  /* ********************************************************************************* */
  void Attach_Content(OffsetBoxBase *content) {
    this->ContentOBox = content;
  }
  /* ********************************************************************************* */
  Graphic_OffsetBox* Spawn_OffsetBox() override {
    Graphic_OffsetBox *grbox = new Graphic_OffsetBox();// Spawn an OffsetBox specific to this type of songlet.
    grbox->Attach_Songlet(this);
    return grbox;
  }
  /* ********************************************************************************* */
  void Draw_Me(IDrawingContext& ParentDC) override {
    if (this->MyArtist!=nullptr){
      this->MyArtist->Draw_Me(ParentDC);
    }
  }
  CajaDelimitadora* GetBoundingBox() override { return nullptr; }
  void UpdateBoundingBox() override {}
  void UpdateBoundingBoxLocal() override {}
  /* ********************************************************************************* */
  void GoFishing(Grabber& Scoop) {}
  /* ********************************************************************************* */
  GraphicBox* Clone_Me() override { return nullptr; }
//  GraphicBox* Clone_Me() override {
//    return new GraphicBox();// nullptr;
//  }
//  IDrawable* Clone_Me() override {
//    return new GraphicBox();// nullptr;
//  }
  /* ********************************************************************************* */
  GraphicBox* Deep_Clone_Me(CollisionLibrary& HitTable) override { return nullptr; }
  /* ********************************************************************************* */
  void Copy_From(GraphicBox& donor) {}
  /* ********************************************************************************* */
  void Draw_Grid(IDrawingContext& ParentDC) {// to do: migrate this to a master drawing utility
    double xloc, yloc;
    double MinX, MinY, MaxX, MaxY;// in audio coordinates
    int ScreenMinX, ScreenMinY, ScreenMaxX, ScreenMaxY;// in screen coordinates
    int X0, Y0;
    int width, height;

    MinX = Math::floor(ParentDC.ClipBounds.Min.x);
    MinY = Math::floor(ParentDC.ClipBounds.Min.y);
    MaxX = Math::ceil(ParentDC.ClipBounds.Max.x);
    MaxY = Math::ceil(ParentDC.ClipBounds.Max.y);

    ScreenMinX = (int) ParentDC.GlobalOffset->UnMapTime(MinX);
    ScreenMinY = (int) ParentDC.GlobalOffset->UnMapPitch(MinY);
    ScreenMaxX = (int) ParentDC.GlobalOffset->UnMapTime(MaxX);
    ScreenMaxY = (int) ParentDC.GlobalOffset->UnMapPitch(MaxY);

    if (ScreenMaxY < ScreenMinY) {// swap
      int temp = ScreenMaxY;
      ScreenMaxY = ScreenMinY;
      ScreenMinY = temp;
    }

    width = ScreenMaxX - ScreenMinX;
    height = ScreenMaxY - ScreenMinY;
    /*
        ParentDC.gr.setColor(Globals.ToAlpha(Color.lightGray, 100));// draw minor horizontal pitch lines
        for (double ysemi = MinY; ysemi < MaxY; ysemi += 1.0 / 12.0) {// semitone lines
          yloc = ParentDC.GlobalOffset.UnMapPitch(ysemi);
          ParentDC.gr.drawLine(ScreenMinX, (int) yloc, ScreenMaxX, (int) yloc);
        }

        ParentDC.gr.setColor(Globals.ToAlpha(Color.lightGray, 100));// draw minor vertical time lines
        for (double xsemi = MinX; xsemi < MaxX; xsemi += 1.0 / 4.0) {// 1/4 second time lines
          xloc = ParentDC.GlobalOffset.UnMapTime(xsemi);
          ParentDC.gr.drawLine((int) xloc, ScreenMinY, (int) xloc, ScreenMaxY);
        }

        Stroke PrevStroke = ParentDC.gr.getStroke();
        BasicStroke bs = new BasicStroke(2.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
        ParentDC.gr.setStroke(bs);
        ParentDC.gr.setColor(Globals.ToAlpha(Color.darkGray, 100));// draw major horizontal pitch lines
        for (double ycnt = MinY; ycnt < MaxY; ycnt++) {// octave lines
          yloc = ParentDC.GlobalOffset.UnMapPitch(ycnt);
          ParentDC.gr.drawLine(ScreenMinX, (int) yloc, ScreenMaxX, (int) yloc);
        }

        ParentDC.gr.setColor(Globals.ToAlpha(Color.darkGray, 100));// draw major vertical time lines
        for (double xcnt = MinX; xcnt < MaxX; xcnt++) {// 1 second time lines
          xloc = ParentDC.GlobalOffset.UnMapTime(xcnt);
          ParentDC.gr.drawLine((int) xloc, ScreenMinY, (int) xloc, ScreenMaxY);
        }

        // draw origin lines
        ParentDC.gr.setColor(Globals.ToAlpha(Color.red, 255));
        X0 = (int) ParentDC.GlobalOffset.UnMapTime(0);
        ParentDC.gr.drawLine(X0, ScreenMinY, X0, ScreenMaxY);

        Y0 = (int) ParentDC.GlobalOffset.UnMapPitch(0);
        ParentDC.gr.drawLine(ScreenMinX, Y0, ScreenMaxX, Y0);

        ParentDC.gr.setStroke(PrevStroke);
    */
  }
  /* ********************************************************************************* */
  void AntiAlias(Graphics2D& g2d) {}
  /* ********************************************************************************* */
  boolean Create_Me() override { return 0; }// IDeletable
  void Delete_Me() override {// IDeletable
    this->MyBounds.Delete_Me();
    delete this->ContentOBox;//.Delete_Me();
    this->Floater = nullptr;// wreck everything
    this->RefCount = Integer::MIN_VALUE;
    this->FreshnessTimeStamp = Integer::MIN_VALUE;
  }
  /* ********************************************************************************* */
//  int Ref_Songlet() override { return 0; } // ISonglet
//  int UnRef_Songlet() override { return 0; }
//  int GetRefCount() override { return 0; }
  /* ********************************************************************************* */
  JsonParse::HashNode* Export(CollisionLibrary& HitTable) override { return nullptr; }
  void ShallowLoad(JsonParse::HashNode& phrase) override {}
  void Consume(JsonParse::HashNode& phrase, CollisionLibrary& ExistingInstances) override {}
  /* ********************************************************************************* */
  // this is all junk that is never used because GraphicBox does not play audio
  SingerBase* Spawn_Singer() override { return nullptr; }
  int Get_Sample_Count(int SampleRate) { return 0; }
  //double Update_Durations() override { return 0; }
  double Get_Duration() override { return 0; }
  double Get_Max_Amplitude() override { return 0; }
  void Update_Guts(MetricsPacket& metrics) override {
    ISonglet *songlet = this->ContentOBox->GetContent();
    songlet->Update_Guts(metrics);
  }
  void Refresh_Me_From_Beneath(IMoveable& mbox) override {};// should be just for IContainer types, but aren't all songs containers?
  /* ********************************************************************************* */
  void Sort_Me() {}
  AudProject* Get_Project() { return nullptr; }
  void Set_Project(Config* project) override {
    this->MyProject = project;
    // below here may be redundant with Update_Guts
    ISonglet *songlet = this->ContentOBox->GetContent();
    songlet->Set_Project(this->MyProject);
  }
  /* ********************************************************************************* */
  void SetMute(boolean Mute) {}
  /* ********************************************************************************* */
  int FreshnessTimeStamp_g() { return 0; }
  void FreshnessTimeStamp_s(int TimeStampNew) {}
};

GraphicBox::ArtistFactory *GraphicBox::ArtistMaker = nullptr;// for static declaration

#endif
