#ifndef ISonglet_hpp
#define ISonglet_hpp

#include "IDeletable.hpp"
#include "ITextable.hpp"
#include "IDrawable.hpp"
#include "Config.hpp"

class OffsetBoxBase;// forward
class SingerBase; // forward
class MetricsPacket;// forward

class ISonglet: public IDrawable, public IDeletable, public ITextable {// Cancionita
public:
  Config *MyProject = nullptr;
  int FreshnessTimeStamp;
  int RefCount = 0;
  virtual ~ISonglet() {};
  /* ********************************************************************************* */
  virtual OffsetBoxBase* Spawn_OffsetBox() = 0;// for compose time
  /* ********************************************************************************* */
  virtual SingerBase* Spawn_Singer() = 0;// for render time

  /* ********************************************************************************* */
  virtual SoundFloat Get_Duration() = 0;
  /* ********************************************************************************* */
  virtual SoundFloat Get_Max_Amplitude() = 0;
  /* ********************************************************************************* */
  virtual void Update_Guts(MetricsPacket& metrics) = 0;
  /* ********************************************************************************* */
  virtual void Refresh_Me_From_Beneath(IMoveable& mbox) = 0;// should be just for IContainer types, but aren't all songs containers?
  /* ********************************************************************************* */
  virtual void Set_Project(Config* project) { this->MyProject = project; }
  /* ********************************************************************************* */
  //@Override ISonglet Deep_Clone_Me(ITextable.CollisionLibrary HitTable) = 0;
  /* ********************************************************************************* */
  virtual int Ref_Songlet() {// ISonglet Reference Counting: increment ref counter and return neuvo value just for kicks
    return ++this->RefCount;
  }
  virtual int UnRef_Songlet() {// ISonglet Reference Counting: decrement ref counter and return neuvo value just for kicks
    if (this->RefCount < 0) {
      throw std::runtime_error("Voice: Negative RefCount:" + this->RefCount);
    }
    return --this->RefCount;
  }
  virtual int GetRefCount() {// ISonglet Reference Counting: get number of references for serialization
    return this->RefCount;
  }
  /* ********************************************************************************* */
  static int Unref(ISonglet** SongletPointerPointer){// Failed(?) experiment. Should probably delete this.
    ISonglet *songlet = *SongletPointerPointer;
    if (songlet==nullptr){ return -1; }
    int NumLeft = songlet->UnRef_Songlet();
    if (NumLeft<=0){
       delete songlet;
       *SongletPointerPointer = nullptr;
    }
    return NumLeft;
  }
};

class IContainer: public ISonglet {
public:
  /* ********************************************************************************* */
  //virtual void Refresh_Me_From_Beneath(IMoveable& mbox) = 0;// should be just for IContainer types, but aren't all songs containers?
  void Refresh_Me_From_Beneath(IMoveable& mbox) override {};// should be just for IContainer types, but aren't all songs containers?
};

/* ********************************************************************************* */
class MetricsPacket {
public:
  SoundFloat MaxDuration = 0.0;
  Config* MyProject = nullptr;
  int FreshnessTimeStamp = 1;
  void Reset(){
    MaxDuration = 0.0;
    FreshnessTimeStamp = 1;
  }
};

#endif // ISonglet_hpp

