#ifndef ITextable_hpp
#define ITextable_hpp

#include <iostream>
#include <sstream>  // Required for stringstreams
#include <string>
#include <vector>
#include "Globals.hpp"
#include "JsonParse.hpp"

/**
 *
 * @author MultiTool
*/
class CollisionLibrary;// forward
class ITextable {// DIY Json ISerializable - more control
public:
  StringConst ObjectTypeName = "ObjectTypeName";// for serialization
  virtual JsonParse::HashNode* Export(CollisionLibrary& HitTable) { return nullptr; };// pass a collision table parameter
  virtual void ShallowLoad(JsonParse::HashNode& phrase){};// just fill in primitive fields that belong to this object, don't follow up pointers.
  virtual void Consume(JsonParse::HashNode& phrase, CollisionLibrary& ExistingInstances){};// Fill in all the values of an already-created object, including deep pointers.
//  IFactory GetMyFactory();// this always returns a singleton of IFactory, one for each class declaration.
//
  /* ********************************************************************************* */
  static double GetNumberField(JsonParse::HashNode &hnode, const String FieldName, double DefaultValue){
    String FieldTxt;
    if (hnode.TryGetField(FieldName, FieldTxt)){
      return Double::ParseDouble(FieldTxt, DefaultValue);
    }else{
      return DefaultValue;
    }
  }
  /* ********************************************************************************* */
  static bool GetBoolField(JsonParse::HashNode &hnode, const String FieldName, bool DefaultValue){
    String FieldTxt;
    if (hnode.TryGetField(FieldName, FieldTxt)){
      return Boolean::ParseBoolean(FieldTxt, DefaultValue);
    }else{
      return DefaultValue;
    }
  }
  /* ********************************************************************************* */
  static String GetStringField(JsonParse::HashNode &hnode, const String FieldName, String DefaultValue){
    String FieldTxt;
    if (hnode.TryGetField(FieldName, FieldTxt)){
      return FieldTxt;
    }else{
      return DefaultValue;
    }
  }
  static JsonParse::LiteralNode* PackNumberField(double Value) {
    JsonParse::LiteralNode *phrase = new JsonParse::LiteralNode();
    //phrase->Literal = String.valueOf(Value); // snox, fill this in
    return phrase;
  }
  static JsonParse::LiteralNode* PackBoolField(bool Value) {
    JsonParse::LiteralNode *phrase = new JsonParse::LiteralNode();
    //phrase->Literal = String.valueOf(Value); // snox, fill this in
    return phrase;
  }
  static JsonParse::LiteralNode* PackStringField(const String &Value) {
    JsonParse::LiteralNode *phrase = new JsonParse::LiteralNode();
    phrase->Literal = Value;
    return phrase;
  }
  template<typename TextableT>
  static void MakeArray(CollisionLibrary &HitTable, ArrayList<TextableT*> &Things, JsonParse::ArrayNode &Parent) {
    int len = Things.size();
    for (int cnt = 0; cnt < len; cnt++) {
      Parent.AddSubPhrase(Things.get(cnt)->Export(HitTable));
    }
  }
  static bool IsTxtPtr(String txt){
    return true; // snox to do: fill this in.
  }
};

/* ********************************************************************************* */
class CollisionItem {// do we really need this?
public:
  String ItemTxtPtr;// Key, usually
  ITextable *Hydrated = nullptr;//, ClonedItem = null;
  JsonParse::HashNode *JsonPhrase = nullptr;// serialization of the ITextable Item
};

class CollisionLibrary {// contains twice-indexed list of instances of (usually) songlet/phrase pairs for serialization, DEserialization, and cloning
public:
  int ItemIdNum = 0;
  HashMap<ITextable*, CollisionItem*> Instances;// serialization and cloning
  HashMap<String, CollisionItem*> Items;// DEserialization
  ~CollisionLibrary(){
    this->Clear();
  }
  void Clear(){
    // to do: iterate through my hashmaps and delete all CollisionItems, but not the Items they point to.
    //std::map<String, CollisionItem*>::iterator iter;
    for (auto iter=Items.begin(); iter!=Items.end(); ++iter){
      delete iter->second;
    }
    Items.clear();
    this->Instances.clear();
  }
  CollisionItem *InsertUniqueInstance(ITextable *KeyObj) {// for serialization
    CollisionItem *ci = new CollisionItem();
//    ci.ItemTxtPtr = Globals.PtrPrefix + ItemIdNum;
//    ci.Hydrated = KeyObj;
//    this->Instances.put(KeyObj, ci);// object is key
//    this->Items.put(ci.ItemTxtPtr, ci);// string is key
//    ItemIdNum++;
    return ci;
  }
  void InsertTextifiedItem(const String &KeyTxt, JsonParse::HashNode *JsonNode) {// for deserialization, only on load
    CollisionItem *ci = new CollisionItem();
    ci->ItemTxtPtr = KeyTxt; ci->JsonPhrase = JsonNode;
    ci->Hydrated = nullptr;
    this->Items.put(KeyTxt, ci);// string is key
  }
  CollisionItem* GetItem(ITextable* KeyObj) {// for serialization
    return nullptr;
    //return this->Instances.get(KeyObj);
  }
  CollisionItem* GetItem(String& KeyTxt) {
    return this->Items.get(KeyTxt);
  }
  JsonParse::HashNode* ExportJson() {
    JsonParse::HashNode *MainPhrase = new JsonParse::HashNode();
//    MainPhrase.ChildrenHash = new HashMap<String, JsonParse.Node>();
//    JsonParse.Node ChildPhrase;
//    CollisionItem ci;
//    for (Map.Entry<String, CollisionItem> entry : this->Items.entrySet()) {
//      ci = entry.getValue();
//      if (ci.JsonPhrase != null) {
//        //ChildPhrase = new JsonParse.Node();// should we clone the child phrase?
//        MainPhrase.AddSubPhrase(ci.ItemTxtPtr, ci.JsonPhrase);
//      }
//    }
    return MainPhrase;
  }
  void ConsumeLibrary(JsonParse::HashNode& LibraryPhrase) {
    this->Clear();
    //std::map<String, JsonParse::Node*>::iterator iter;
    for (auto iter=LibraryPhrase.ChildrenHash.begin(); iter!=LibraryPhrase.ChildrenHash.end(); ++iter){
      JsonParse::HashNode *JsonNode = (JsonParse::HashNode*)iter->second;// another cast!
      this->InsertTextifiedItem(iter->first, JsonNode);
    }
  }
  void Wipe_Songlets() {// for testing
    CollisionItem *ci;
//    IDeletable deletable;
//    for (Map.Entry<String, CollisionItem> entry : this->Items.entrySet()) {
//      ci = entry.getValue();
//      if ((deletable = (IDeletable) ci.Hydrated) != null) {
//        //deletable.Delete_Me();
//        ci.Hydrated = null;
//      }
//    }
  }
};

#endif
