#ifndef FileMapper_hpp
#define FileMapper_hpp

#include "Globals.hpp"
#include "LoopSong.hpp"
#include "GraphicBox.hpp"
#include "PluckVoice.hpp"

// #include "Vectunes.hpp"

// WIP
// FileMapper is part of deserialization.  It will convert a parsed json tree onto a playable Vectunes tree.
// Every time we upgrade our file format, we will create a new FileMapper to support it, and keep the old FileMapper around for backward compatibility.
class FileMapper {
public:
  StringConst TreePhraseName = "Tree", LibraryPhraseName = "Library";// for serialization
  /* ********************************************************************************* */
  class IFactory {
  public:
    FileMapper *fmap;
    IFactory(FileMapper *fmap0){ this->fmap = fmap0; }
    virtual ~IFactory(){}
    virtual OffsetBoxBase* Deserialize_OffsetBox(JsonParse::HashNode &OboxNode) = 0;
    virtual ISonglet* Deserialize_Songlet(JsonParse::HashNode *node) = 0;
    /* ********************************************************************************* */
    virtual OffsetBoxBase* Deserialize_Stack(JsonParse::HashNode &OboxNode) {// Create and fill in a songlet and its obox, including deep pointers.
      JsonParse::Node *SongletNode = OboxNode.Get(OffsetBoxBase::ContentName);// value of songlet field
      OffsetBoxBase *obox = nullptr;
      ISonglet *songlet;
      if (SongletNode->MyType == JsonParse::Node::Types::IsLiteral){
        JsonParse::LiteralNode *LinkNode = (JsonParse::LiteralNode*)SongletNode; // another cast!
        String ContentTxt = LinkNode->Get();
        if (ITextable::IsTxtPtr(ContentTxt)) {// if songlet content is just a pointer into the library
          CollisionItem *citem = fmap->ExistingInstances.GetItem(ContentTxt);// look up my songlet in the library
          if (citem != nullptr){
            if ((songlet = (ISonglet*)citem->Hydrated) == null) {// another cast!
              citem->Hydrated = songlet = this->Deserialize_Songlet(citem->JsonPhrase);
            }
          } else {
            return nullptr; // Error!  content is a text link but has no item in library table.
          }
        } else {
          return nullptr; // Error! content is text but not a pointer
        }
      } else if (SongletNode->MyType == JsonParse::Node::Types::IsHash){// songlet is inline, inside this one offsetbox
        JsonParse::HashNode *ObjectNode = (JsonParse::HashNode*)SongletNode; // another cast!
        songlet = this->Deserialize_Songlet(ObjectNode);
      } else {
        return nullptr; // Error! content is neither link nor hash object
      }
      obox = songlet->Spawn_OffsetBox();
      //obox->ShallowLoad(OboxNode);
      // Custom-filling the obox is a problem if we ever want to do it from FileMapper. Will need a virtual fn with a cast.
      return obox;
    } //or Get_Songlet_Node?
  };
  /* ********************************************************************************* */
  class VoiceFactory : public IFactory{
  public:
    VoiceFactory(FileMapper *fmap0):IFactory(fmap0){}
    /* ********************************************************************************* */
    Voice::Voice_OffsetBox* Deserialize_OffsetBox(JsonParse::HashNode &OboxNode) override {
      Voice::Voice_OffsetBox* vobox = (Voice::Voice_OffsetBox*)Deserialize_Stack(OboxNode);// another cast!
      // For new file formats, we can replace vobox shallow load here.
      vobox->ShallowLoad(OboxNode);
      return vobox;
    }
    /* ********************************************************************************* */
    Voice* Deserialize_Songlet(JsonParse::HashNode *node) override {
      // Before we even enter this function, first determine if phrase just has a txt pointer instead of a ChildrenHash.
      Voice* voz = new Voice();
      Hydrate_Voice(node, voz);
      return voz;
    }
    /* ********************************************************************************* */
    void Hydrate_Voice(JsonParse::HashNode *VoiceNode, Voice *voz) {// Fill in all the values of an already-created object, including deep pointers.
      if (VoiceNode == null) { return; }
      voz->ShallowLoad(*VoiceNode);
      JsonParse::Node *SubNode = VoiceNode->Get(Voice::CPointsName);
      if (SubNode != null && SubNode->MyType == JsonParse::Node::Types::IsArray) {
        JsonParse::ArrayNode *PhrasePointList = (JsonParse::ArrayNode*)SubNode;// another cast!
        voz->Wipe_CPoints();
        VoicePoint *vpoint;
        JsonParse::HashNode *PhrasePoint;
        int len = PhrasePointList->ChildrenArray.size();
        for (int pcnt = 0; pcnt < len; pcnt++) {
          PhrasePoint = (JsonParse::HashNode*)PhrasePointList->Get(pcnt);// another cast!
          vpoint = new VoicePoint();// to do: replace this with a factory owned by VoicePoint.
          vpoint->Consume(*PhrasePoint, fmap->ExistingInstances);// to do: get rid of Consume and change this instance to shallowload(?)
          voz->Add_Note(vpoint);
        }
        voz->Sort_Me();
      }
    }
  };
  /* ********************************************************************************* */
  class SampleVoiceFactory : public VoiceFactory{
  public:
    StringConst ObjTypeName = "SampleVoice_OffsetBox";
    SampleVoiceFactory(FileMapper *fmap0):VoiceFactory(fmap0){}
    /* ********************************************************************************* */
    SampleVoice::SampleVoice_OffsetBox* Deserialize_OffsetBox(JsonParse::HashNode &OboxNode) override {
      SampleVoice::SampleVoice_OffsetBox* vobox = (SampleVoice::SampleVoice_OffsetBox*)Deserialize_Stack(OboxNode);// another cast!
      // For new file formats, we can replace vobox shallow load here.
      vobox->ShallowLoad(OboxNode);
      return vobox;
    }
    /* ********************************************************************************* */
    SampleVoice* Deserialize_Songlet(JsonParse::HashNode *node) override {
      // Before we even enter this function, first determine if phrase just has a txt pointer instead of a ChildrenHash.
      SampleVoice* voz = new SampleVoice();
      Hydrate_SampleVoice(node, voz);
      return voz;
    }
    /* ********************************************************************************* */
    void Hydrate_SampleVoice(JsonParse::HashNode *VoiceNode, SampleVoice *voz) {// Fill in all the values of an already-created object, including deep pointers.
      //if (VoiceNode == null) { return; }
      Hydrate_Voice(VoiceNode, voz);// SampleVoice overrides shallowload
      //voz->ShallowLoad(*VoiceNode);
    }
  };
  /* ********************************************************************************* */
  class PluckVoiceFactory : public SampleVoiceFactory{
  public:
    PluckVoiceFactory(FileMapper *fmap0):SampleVoiceFactory(fmap0){}
    /* ********************************************************************************* */
    PluckVoice::PluckVoice_OffsetBox* Deserialize_OffsetBox(JsonParse::HashNode &OboxNode) override {
      PluckVoice::PluckVoice_OffsetBox* vobox = (PluckVoice::PluckVoice_OffsetBox*)Deserialize_Stack(OboxNode);// another cast!
      // For new file formats, we can replace vobox shallow load here.
      vobox->ShallowLoad(OboxNode);
      return vobox;
    }
    /* ********************************************************************************* */
    PluckVoice* Deserialize_Songlet(JsonParse::HashNode *node) override {
      // Before we even enter this function, first determine if phrase just has a txt pointer instead of a ChildrenHash.
      PluckVoice* voz = new PluckVoice();
      Hydrate_PluckVoice(node, voz);
      return voz;
    }
    /* ********************************************************************************* */
    void Hydrate_PluckVoice(JsonParse::HashNode *VoiceNode, SampleVoice *voz) {// Fill in all the values of an already-created object, including deep pointers.
      Hydrate_SampleVoice(VoiceNode, voz);// SampleVoice overrides shallowload
      //voz->ShallowLoad(*VoiceNode);
    }
  };
  /* ********************************************************************************* */
  class GroupSongFactory : public IFactory{
  public:
    GroupSongFactory(FileMapper *fmap0):IFactory(fmap0){}
    /* ********************************************************************************* */
    GroupSong::Group_OffsetBox* Deserialize_OffsetBox(JsonParse::HashNode &OboxNode) override {
      GroupSong::Group_OffsetBox* grobox = (GroupSong::Group_OffsetBox*)Deserialize_Stack(OboxNode);// another cast!
      // For new file formats, we can replace grobox shallow load here.
      grobox->ShallowLoad(OboxNode);
      return grobox;
    }
    /* ********************************************************************************* */
    GroupSong* Deserialize_Songlet(JsonParse::HashNode *node) override {
      // Before we even enter this function, first determine if phrase just has a txt pointer instead of a ChildrenHash.
      GroupSong* group = new GroupSong();
      Hydrate_Group(node, group);
      return group;
    }
    /* ********************************************************************************* */
    void Hydrate_Group(JsonParse::HashNode *GroupNode, GroupSong *group) {// Fill in all the values of an already-created object, including deep pointers.
      if (GroupNode == null) { return; }
      group->ShallowLoad(*GroupNode);
      JsonParse::Node *SubNode = GroupNode->Get(GroupSong::SubSongsName);
      if (SubNode != null && SubNode->MyType == JsonParse::Node::Types::IsArray) {
        JsonParse::ArrayNode *ChildPhraseList = (JsonParse::ArrayNode*)SubNode;// another cast!
        group->Wipe_SubSongs();
        OffsetBoxBase *ChildObox;
        JsonParse::Node *ChildItemNode;
        int len = ChildPhraseList->ChildrenArray.size();
        for (int pcnt = 0; pcnt < len; pcnt++) {// iterate through the array
          ChildItemNode = ChildPhraseList->Get(pcnt);
          if (ChildItemNode!=nullptr && ChildItemNode->MyType == JsonParse::Node::Types::IsHash){
            JsonParse::HashNode *ChildObjNode = (JsonParse::HashNode*)ChildItemNode;// another cast!
            String TypeName = ITextable::GetStringField(*ChildObjNode, ITextable::ObjectTypeName, "null");
            IFactory *factory = fmap->GetFactory(TypeName);// use factories to deal with polymorphism
            if (factory!=nullptr){
              ChildObox = factory->Deserialize_OffsetBox(*ChildObjNode);
              group->Add_SubSong(ChildObox);
            }
          }
        }
        group->Sort_Me();
      }
    }
  };
  /* ********************************************************************************* */
  class LoopSongFactory : public GroupSongFactory{
  public:
    LoopSongFactory(FileMapper *fmap0):GroupSongFactory(fmap0){}
    /* ********************************************************************************* */
    LoopSong::Loop_OffsetBox* Deserialize_OffsetBox(JsonParse::HashNode &OboxNode) override {
      LoopSong::Loop_OffsetBox* grobox = (LoopSong::Loop_OffsetBox*)Deserialize_Stack(OboxNode);// another cast!
      // For new file formats, we can replace grobox shallow load here.
      grobox->ShallowLoad(OboxNode);
      return grobox;
    }
    /* ********************************************************************************* */
    LoopSong* Deserialize_Songlet(JsonParse::HashNode *node) override {
      // Before we even enter this function, first determine if phrase just has a txt pointer instead of a ChildrenHash.
      LoopSong* loop = new LoopSong();
      //Hydrate_Loop(node, loop); // this will not actually work
      return loop;
    }
    /* ********************************************************************************* */
    void Hydrate_Loop(JsonParse::HashNode *LoopNode, LoopSong *loop) {// Fill in all the values of an already-created object, including deep pointers.
      if (LoopNode == null) { return; }
      loop->ShallowLoad(*LoopNode);// WARNING Hydrate_Loop is all untested junk shoveled from GroupFactory
      JsonParse::Node *SubNode = LoopNode->Get(GroupSong::SubSongsName);
      if (SubNode != null && SubNode->MyType == JsonParse::Node::Types::IsArray) {
        JsonParse::ArrayNode *ChildPhraseList = (JsonParse::ArrayNode*)SubNode;// another cast!
        loop->Wipe_SubSongs();
        OffsetBoxBase *ChildObox;
        JsonParse::Node *ChildItemNode;
        int len = ChildPhraseList->ChildrenArray.size();
        for (int pcnt = 0; pcnt < len; pcnt++) {// iterate through the array
          ChildItemNode = ChildPhraseList->Get(pcnt);
          if (ChildItemNode!=nullptr && ChildItemNode->MyType == JsonParse::Node::Types::IsHash){
            JsonParse::HashNode *ChildObjNode = (JsonParse::HashNode*)ChildItemNode;// another cast!
            String TypeName = ITextable::GetStringField(*ChildObjNode, ITextable::ObjectTypeName, "null");
            IFactory *factory = fmap->GetFactory(TypeName);// use factories to deal with polymorphism
            if (factory!=nullptr){
              ChildObox = factory->Deserialize_OffsetBox(*ChildObjNode);
              loop->Add_SubSong(ChildObox);
            }
          }
        }
        loop->Sort_Me();
      }
    }
  };
  /* ********************************************************************************* */
  class GraphicFactory: public IFactory {// for serialization
  public:
    GraphicFactory(FileMapper *fmap0):IFactory(fmap0){}
    /* ********************************************************************************* */
    GraphicBox::Graphic_OffsetBox* Deserialize_OffsetBox(JsonParse::HashNode &OboxNode) override {
      GraphicBox::Graphic_OffsetBox* grabox = (GraphicBox::Graphic_OffsetBox*)Deserialize_Stack(OboxNode);// another cast!
      // For new file formats, we can replace grabox shallow load here.
      grabox->ShallowLoad(OboxNode);
      return grabox;
    }
    /* ********************************************************************************* */
    GraphicBox* Deserialize_Songlet(JsonParse::HashNode *node) override {
      // Before we even enter this function, first determine if phrase just has a txt pointer instead of a ChildrenHash.
      GraphicBox *graphic = new GraphicBox();
      Hydrate_Graphic(node, graphic);
      return graphic;
    }
    void Hydrate_Graphic(JsonParse::HashNode *GraphicNode, GraphicBox *grabox){
      if (GraphicNode == null) { return; }
      grabox->ShallowLoad(*GraphicNode);
      JsonParse::Node *SubNode = GraphicNode->Get(GraphicBox::ContentOBoxName);
      if (SubNode != nullptr && SubNode->MyType == JsonParse::Node::Types::IsHash) {
        JsonParse::HashNode *ChildOboxNode = (JsonParse::HashNode*)SubNode;// another cast!
        OffsetBoxBase *ChildObox;
        String TypeName = ITextable::GetStringField(*ChildOboxNode, ITextable::ObjectTypeName, "null");
        IFactory *factory = fmap->GetFactory(TypeName);// use factories to deal with polymorphism
        if (factory!=nullptr){
          ChildObox = factory->Deserialize_OffsetBox(*ChildOboxNode);
          grabox->Attach_Content(ChildObox);
        }
      }
    }
  };
  /* ********************************************************************************* */
  CollisionLibrary HitTable;
  //HashMap<String, ISonglet*> HitTable;
  CollisionLibrary ExistingInstances;
  HashMap<String, IFactory*> Factories;
  /* ********************************************************************************* */
  FileMapper(){
    this->BuildFactories();
    ExistingInstances.Clear();
    HitTable.Clear();
  }
  ~FileMapper(){
    this->DeleteFactories();
  }
  /* ********************************************************************************* */
  static double GetNumberField(JsonParse::HashNode *hnode, const String FieldName, double DefaultValue){
    String FieldTxt;
    if (hnode->TryGetField(FieldName, FieldTxt)){
      return Double::ParseDouble(FieldTxt, DefaultValue);
    }else{
      return DefaultValue;
    }
  }
  /* ********************************************************************************* */
  static void FillOffsetBox(OffsetBoxBase *obox, JsonParse::HashNode *hnode){
    obox->TimeX = GetNumberField(hnode, MonkeyBox::TimeXName, 0);
    obox->OctaveY = GetNumberField(hnode, MonkeyBox::OctaveYName, 0);
    obox->LoudnessFactor = GetNumberField(hnode, MonkeyBox::LoudnessFactorName, 1.0);
    obox->ScaleX = GetNumberField(hnode, MonkeyBox::ScaleXName, 1.0);
    obox->ScaleY = GetNumberField(hnode, MonkeyBox::ScaleYName, 1.0);
    if (false){
      obox->OctavesPerRadius = GetNumberField(hnode, "OctavesPerRadius", 0.01);
    }
  }
  /* ********************************************************************************* */
  void BuildFactories(){
    Factories.put("Graphic_OffsetBox", new GraphicFactory(this));
    Factories.put("Voice_OffsetBox", new VoiceFactory(this));
    Factories.put(SampleVoiceFactory::ObjTypeName, new SampleVoiceFactory(this));
    Factories.put(PluckVoice::PluckVoice_OffsetBox::ObjectTypeName, new PluckVoiceFactory(this));
    Factories.put("Group_OffsetBox", new GroupSongFactory(this));
    Factories.put(LoopSong::Loop_OffsetBox::ObjectTypeName, new LoopSongFactory(this));
  }
  /* ********************************************************************************* */
  void DeleteFactories(){
    std::map<String, IFactory*>::iterator iter;
    for (iter=this->Factories.begin(); iter!=this->Factories.end(); ++iter){
      delete iter->second;
    }
    this->Factories.clear();
  }
  /* ********************************************************************************* */
  IFactory* GetFactory(const String &FactoryName){
    IFactory *factory;
    if (this->Factories.ContainsKey(FactoryName)){
      factory = this->Factories.get(FactoryName);
      return factory;
    }
    return nullptr;
  }
  /* ********************************************************************************* */
  GraphicBox::Graphic_OffsetBox* Create(JsonParse::HashNode *RootObjNode){
    OffsetBoxBase *obox;
    GraphicBox::Graphic_OffsetBox *RootSongObox = nullptr;

    // Retrieve these hashnodes.
    JsonParse::HashNode *TreePhrase = (JsonParse::HashNode*)RootObjNode->Get(TreePhraseName);// another cast!
    JsonParse::HashNode *LibraryPhrase = (JsonParse::HashNode*)RootObjNode->Get(LibraryPhraseName);// another cast!

    this->ExistingInstances.ConsumeLibrary(*LibraryPhrase);

    String TypeName = ITextable::GetStringField(*TreePhrase, ITextable::ObjectTypeName, "null");
    IFactory *factory = this->GetFactory(TypeName);// use factories to deal with polymorphism
    if (factory!=nullptr){
      // here we presume that the root obox type was a Graphic_OffsetBox.
      // to do: put some safeguards in here in case it isn't.
      RootSongObox = (GraphicBox::Graphic_OffsetBox*)factory->Deserialize_OffsetBox(*TreePhrase);// another cast!
    }
    return RootSongObox;
/*
each creator would: 1. receive obox node, but already knows its own type
2. create or look up songlet content.
3. create obox, fill it in from node.  *allows custom obox fill in*
*/
  }
};

#endif // FileMapper_hpp

/*
the legacy file format involves offset boxes doing the real factory work.
e g if json is tagged voice_offsetbox, factory creates a voice_offsetbox, then the voice_offsetbox is asked to create a voice.
this meant an inverted creation, as voice_offsetbox is *supposed* to only be born from a voice, and attached to it. not the other way around.

can filemapper make it better?  maybe each creator can detect the obox type, then create the songlet, and the songlet creates the correct obox.
the songlet creation can be de novo or from the hit table, but either way it makes a new obox.
so, 1 detect, 2 create songlet, 3 FILL songlet with data, 4. (on the way back?) create and FILL the songlet's obox.
any collision issues?

must first check obox properties for "ptr:x" content before choosing to fetch or create a new songlet. can do this before creating obox?
GenericOboxParser(Node oboxnode){
  check for obox objecttypename and determine my type of songlet.
  check for Node.Content literal, if ptr then look into hit table, fetch, create obox (auto attached) and return obox.
  if NOT ptr, create new songlet, fill songlet data, spawn new obox (auto attached), and return obox.
}

ok again,
deserialize library first? then do whole tree?

*/

