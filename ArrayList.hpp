#ifndef ArrayList_hpp
#define ArrayList_hpp

#include <vector>
#include "Globals.hpp"

/*
Starting to move fake Java code into ArrayList.hpp.  Eventually will rename this file JavaFake.hpp?
*/

#define String std::string
//class String : public std::string {
//public:
// //String substring(int startIndex, int endIndex)
//};

class Graphics2D {
};

class Color {
public:
  Color() {};
  Color(SoundFloat R, SoundFloat G, SoundFloat B) {};
  Color(SoundFloat R, SoundFloat G, SoundFloat B, SoundFloat A) {};
  SoundFloat getRed() { return 0.0; };
  SoundFloat getGreen() { return 0.0; };
  SoundFloat getBlue() { return 0.0; };

  //* ********************************************************************************* *
  static Color* ToAlpha(Color& col, int Alpha) {
    return new Color(col.getRed(), col.getGreen(), col.getBlue(), Alpha);// rgba
  }
  //* ********************************************************************************* *
  static Color* ToRainbow(double Fraction) {
    if (Fraction < 0.5) {
      Fraction *= 2;
      return new Color((1.0 - Fraction), Fraction, 0);
    } else {
      Fraction = Math_min((Fraction - 0.5) * 2, 1.0);
      return new Color(0, (1.0 - Fraction), Fraction);
    }
  }
};

class Double {// more simulated Java
public:
  static constexpr double POSITIVE_INFINITY = std::numeric_limits<double>::max();
  static constexpr double NEGATIVE_INFINITY = std::numeric_limits<double>::min();
  /* ********************************************************************************* */
  static double ParseDouble(const String &NumTxt, double Default){
    int len = NumTxt.size();// to do: clean this mess up
    char First[len+1], *EndPtr;
    NumTxt.copy(First, len);
    First[len] = 0x0;// null terminated
    double Num = strtod(First, &EndPtr);
    int diff = EndPtr - First;
    if (diff==0){Num = Default;}
    //printf("diff:%i\n", diff);
    return Num;
  }
};

class Integer {// more simulated Java
public:
  static constexpr int MAX_VALUE = std::numeric_limits<int>::max();
  static constexpr int MIN_VALUE = std::numeric_limits<int>::min();
};

class Boolean {
public:
  /* ********************************************************************************* */
  static bool ParseBoolean(const String &BoolTxt, bool Default){
    if (BoolTxt.size()<=0) {return Default; }
    String Dupe = BoolTxt;
    //std::transform(Dupe.begin(), Dupe.end(), Dupe.begin(), ::tolower);
    char ch = Dupe.at(0);
    ch = tolower(ch);
    if (ch=='t') { return true; }
    if (ch=='f') { return false; }
    return Default;
  }
};

class Math {// more simulated Java
public:
  static constexpr long double PI = PIdef;
  static constexpr long double E = 2.71828182845904523536028747135266249775724709369995;

  static SoundFloat min(SoundFloat a, SoundFloat b) {
    return std::min(a, b);
  }
  static SoundFloat max(SoundFloat a, SoundFloat b) {
    return std::max(a, b);
  }
  static SoundFloat ceil(SoundFloat a) {
    return std::ceil(a);
  }
  static SoundFloat floor(SoundFloat a) {
    return std::floor(a);
  }
  static SoundFloat round(SoundFloat a) {
    return std::round(a);
  }
  static SoundFloat abs(SoundFloat a) {
    return std::abs(a);
  }
  static SoundFloat hypot(SoundFloat a, SoundFloat b) {
    return std::hypot(a, b);
  }
  static SoundFloat sqrt(SoundFloat a) {
    return std::sqrt(a);
  }
  static SoundFloat sin(SoundFloat a) {
    return std::sin(a);
  }
  static SoundFloat cos(SoundFloat a) {
    return std::cos(a);
  }
  static SoundFloat log(SoundFloat a) {
    return std::log(a);
  }
  static SoundFloat pow(SoundFloat a, SoundFloat exp) {
    return std::pow(a, exp);
  }
  //  std::srand(std::time(nullptr)); // use current time as seed for random generator
  static SoundFloat frand() {
    int numer = std::rand();
    int denom = 9973;//117;// prime number
    int remainder = numer % denom;
    return ((SoundFloat)remainder) / (SoundFloat)denom;
    //return ((SoundFloat)std::rand()) / (SoundFloat)RAND_MAX;
  }
};

template <class Type>
class ArrayList: public std::vector<Type> {
public:
  void remove(const Type item){// remove by content
    typename ArrayList<Type>::iterator it;
    it = std::find(this->begin(), this->end(), item);
    if (it != this->end()){
      this->erase(it);
    }
  }
  void add(const Type item){
    this->push_back(item);
  }
  void Add(const Type item){
    this->push_back(item);
  }
  void Insert(int Dex, Type item){// insert by index
    this->insert(this->begin() + Dex, item);
  }
  Type get(int Dex){
    return this->at(Dex);
  }
  bool Contains(const Type item){// find by content
    typename ArrayList<Type>::iterator it;
    it = std::find(this->begin(), this->end(), item);
    if (it != this->end()){
      return true;
    }else{
      return false;
    }
  }
};

template <class KeyT, class ValueT>
class HashMap: public std::map<KeyT, ValueT> {
public:
  ValueT get(const KeyT &Key){
    return this->find(Key)->second;
  }
  void put(const KeyT &Key, ValueT Value){
    this->insert(std::make_pair(Key, Value));
  }
  //typedef typename std::map<KeyT, ValueT>::const_iterator MapIterator;
  bool ContainsKey(const KeyT& Name){
    int num = this->count(Name);// this is a horrid inefficient approach but the only one that compiles for generics.
    if (num > 0) {return true;}
    else { return false; }

//std::map<KeyT, ValueT>::iterator iter(this->lower_bound(Name));
//if (iter == this->end() || Name < iter->first) {    // not found
//    //this->insert(iter, make_pair(Name, value));     // hinted insertion
//} else {
//    // ... use iter->second here
//}

//    if (this->find(Name) == this->end){ return false; }
//    else { return true; }
    //template <class KeyT, class ValueT>
    //typename std::map<KeyT, ValueT>::iterator iter;
    //typename
    //typename HashMap<KeyT, ValueT>::iterator iter = this->find(Name);
    //MapIterator iter;
    //iter = this->find(Name);
//    if (iter == this->end){ return false; }
//    else { return true; }
    //return this->contains(Name);
  }
};

#endif // ArrayList_hpp
