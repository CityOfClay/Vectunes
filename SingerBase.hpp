#ifndef SingerBase_hpp
#define SingerBase_hpp

#include "Wave.hpp"
#include "OffsetBoxBase.hpp"
#include "IDeletable.hpp"
#include "Config.hpp"

/* ********************************************************************************* */
class SingerBase: public IDeletable {// Cantante
public:// Cantante
  Config *MyProject = nullptr;
  SoundFloat Inherited_OctaveRate = 0.0;// bend context, change dynamically while rendering. not used yet.
  /*
  InheritedMap breakdown:
  Inherited_Time = 0.0, Inherited_Octave = 0.0, Inherited_Loudness = 1.0;// time, octave, and loudness context
  Inherited_ScaleX = 1.0;// tempo rescale context
  Inherited_ScaleY = 1.0;// 'temper' context, which we will NEVER use unless we want to make ugly anharmonic noise.
  */
  MonkeyBox InheritedMap;// InheritedMap is transformation to and from samples. Replacement for Inherited_*
  int SampleRate;

  boolean IsFinished = false;
  SingerBase* ParentSinger = nullptr;
  OffsetBoxBase* MyOffsetBox = nullptr;
  /* ********************************************************************************* */
  SingerBase(){ this->Create_Me(); }
  /* ********************************************************************************* */
  virtual ~SingerBase(){ this->Delete_Me(); }
  /* ********************************************************************************* */
  virtual void Start() = 0;
  /* ********************************************************************************* */
  virtual void Skip_To(SoundFloat EndTime) = 0;
  /* ********************************************************************************* */
  virtual void Render_To(SoundFloat EndTime, Wave& wave) = 0;
  /* ********************************************************************************* */
  virtual void Inherit(SingerBase& parent) {// accumulate transformations
    this->ParentSinger = &parent;
    this->InheritedMap.Copy_From(parent.InheritedMap);
    this->Compound();
  };
  /* ********************************************************************************* */
  virtual void Set_Project(Config *config){
    if (config!=nullptr){
      this->MyProject = config;
      this->SampleRate = config->SampleRate;
    }else{
      printf("Singer Set_Project config is null!\n");
    }
  }
  /* ********************************************************************************* */
  virtual void Compound() {// accumulate my own transformation
    this->Compound(*(this->Get_OffsetBox()));
  };
  /* ********************************************************************************* */
  virtual void Compound(MonkeyBox& donor) {// accumulate my own transformation
    this->InheritedMap.Compound(donor);// to do: combine matrices here.
  };
  /* ********************************************************************************* */
  virtual OffsetBoxBase* Get_OffsetBox() {
    return this->MyOffsetBox;
  };
  /* ********************************************************************************* */
  bool Create_Me() override {// IDeletable
    return true;
  };
  void Delete_Me() override {// IDeletable
    this->MyProject = null;// wreck everything
    this->Inherited_OctaveRate = Double_NEGATIVE_INFINITY;
    this->InheritedMap.Delete_Me();// should be redundant
    this->IsFinished = true;
    this->ParentSinger = null;
    this->MyOffsetBox = null;
  };
};

#endif // SingerBase_hpp
